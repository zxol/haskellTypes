data HowTall =
    Tall |
    Medium |
    Short
    deriving (Eq, Show)

data WherePenginsLive =
    Galapagos |
    Antarctica |
    Australia |
    SouthAfrica |
    SouthAmerica
    deriving (Eq, Show)

data Pengin = Pengin WherePenginsLive HowTall
    deriving (Eq, Show)

isSouthAfrica :: WherePenginsLive -> Bool
isSouthAfrica SouthAfrica = True
isSouthAfrica _ = False

gimmeWhereTheyLive :: Pengin -> WherePenginsLive
gimmeWhereTheyLive (Pengin whereitlives _) = whereitlives

gimmeHowTall :: Pengin -> HowTall
gimmeHowTall (Pengin _ howtall) = howtall

humboldt = Pengin SouthAmerica Short
gentoo = Pengin Antarctica Medium
macaroni = Pengin Australia Tall
little = Pengin Australia Tall
galapagos = Pengin Galapagos Short

allPengins = [ humboldt, gentoo, macaroni, little, galapagos ]

isTallPengin :: Pengin -> Bool
isTallPengin (Pengin _ Tall) = True
isTallPengin _ = False

isMediumPengin :: Pengin -> Bool
isMediumPengin p
    | gimmeHowTall p == Medium = True
    | otherwise = False

isShortPengin(Pengin _ howtall)
    | howtall==Short = True
    | otherwise      = False

isGalapagosPengin :: Pengin -> Bool
isGalapagosPengin (Pengin Galapagos _) = True
isGalapagosPengin _ = False

isAntarcticaPengin :: Pengin -> Bool
isAntarcticaPengin (Pengin Antarctica _) = True
isAntarcticaPengin _ = False

antarcticOrGalapagos :: Pengin -> Bool
antarcticOrGalapagos p = isGalapagosPengin p || isAntarcticaPengin p

main = print $ map isMediumPengin allPengins
